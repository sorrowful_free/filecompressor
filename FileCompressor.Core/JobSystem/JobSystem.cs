using System;
using System.Collections.Generic;
using System.Threading;
using FileCompressor.Core.JobSystem.JobHandler;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.JobSystem
{
    public class JobSystem : IJobSystem
    {
        private readonly int _workerCount;
        private readonly List<JobSystemWorker> _workers;
        private readonly CancellationTokenSource _abortCancellationTokenSource;

        private int _currentWorkerIndex = 0;

        public JobSystem(int workerCount, CancellationToken userCancellationToken)
        {
            _workerCount = workerCount;
            _workers = new List<JobSystemWorker>(workerCount);

            _abortCancellationTokenSource = new CancellationTokenSource();
            var linkedCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(userCancellationToken, _abortCancellationTokenSource.Token);

            for (var i = 0; i < workerCount; i++)
            {
                _workers.Add(new JobSystemWorker(linkedCancellationTokenSource.Token));
            }
        }

        public IJobHandler ExecuteJob(IJob job)
        {
            var worker = _workers[Interlocked.Increment(ref _currentWorkerIndex) % _workerCount];
            var handler = default(IJobHandler);
            try
            {
                handler = worker.ExecuteJob(job);
            }
            catch
            {
                _abortCancellationTokenSource.Cancel();
                throw;
            }

            return handler;
        }

        public void Complete()
        {
            try
            {
                foreach (var worker in _workers)
                {
                    worker.Complete();
                }
            }
            catch
            {
                _abortCancellationTokenSource.Cancel();
                throw;
            }
            finally
            {
                _currentWorkerIndex = 0; //reset index
            }
        }

        public void Abort()
        {
            _abortCancellationTokenSource.Cancel();
            _currentWorkerIndex = 0; //reset index
        }

        public void Dispose()
        {
            Abort();
            foreach (var worker in _workers)
            {
                worker.Dispose();
            }
        }
    }
}