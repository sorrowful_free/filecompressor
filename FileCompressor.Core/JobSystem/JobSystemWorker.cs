using System;
using System.Collections.Concurrent;
using System.Threading;
using FileCompressor.Core.JobSystem.JobHandler;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.JobSystem
{
    internal class JobSystemWorker : IDisposable
    {
        private readonly CancellationToken _cancellationToken;
        private readonly ConcurrentBag<JobHandler.JobHandler> _jobs;
        private readonly ManualResetEvent _completeEvent;
        private readonly JobSystemExceptionHolder _exceptionHolder;
        private readonly Thread _workerThread;


        private bool _isDisposed;

        public JobSystemWorker(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            _jobs = new ConcurrentBag<JobHandler.JobHandler>();
            _completeEvent = new ManualResetEvent(false);
            _exceptionHolder = new JobSystemExceptionHolder();
            _workerThread = new Thread(ProcessJobs);
            _workerThread.Start();
        }

        public IJobHandler ExecuteJob(IJob job)
        {
            _exceptionHolder.RethrowException();
            if (_isDisposed)
            {
                return null;
            }

            var jobHandler = new JobHandler.JobHandler(job);
            _jobs.Add(jobHandler);
            _completeEvent.Reset();
            return jobHandler;
        }

        public void Complete()
        {
            if (!_isDisposed && _jobs.Count > 0)
            {
                _completeEvent.WaitOne();
            }

            _exceptionHolder.RethrowException(); //show inner exception from thread to caller
        }

        public void Dispose()
        {
            _isDisposed = true;
            _workerThread.Join();
            _completeEvent.Dispose();
            _exceptionHolder.RethrowException();
        }

        private void ProcessJobs()
        {
            try
            {
                while (!_isDisposed && !_cancellationToken.IsCancellationRequested)
                {
                    if (_jobs.Count > 0 && _jobs.TryTake(out var job))
                    {
                        job.ExecuteJob(_cancellationToken);
                    }
                    else
                    {
                        if (!_isDisposed)
                        {
                            _completeEvent.Set();
                        }
                    }

                    Thread.Sleep(0);
                }
            }

            catch (Exception exception)
            {
                _exceptionHolder.CatchException(exception);
            }
        }
    }
}