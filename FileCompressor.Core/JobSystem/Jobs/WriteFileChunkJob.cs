using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using FileCompressor.Core.Data;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public readonly struct WriteFileChunkJob : IJob
    {
        private readonly string _filePath;
        private readonly long _filePosition;
        private readonly Chunk _chunk;
        private readonly ConcurrentBag<ArchiveChunk> _output;

        public WriteFileChunkJob(string filePath, long filePosition, Chunk chunk, ConcurrentBag<ArchiveChunk> output = null)
        {
            _filePath = filePath;
            _filePosition = filePosition;
            _chunk = chunk;
            _output = output;
        }

        public void Execute(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            using (var fileStream = File.Open(_filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write))
            {
                fileStream.Position = _filePosition;
                fileStream.Write(_chunk.Data);
                if (_output != null)
                {
                    _output.Add(new ArchiveChunk(_chunk.Index,_filePosition,_chunk.Data.Length));    
                }
                fileStream.Close();
            }
        }
    }
}