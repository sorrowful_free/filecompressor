using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using FileCompressor.Core.Data;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public readonly struct ReadArchivedChunksJob : IJob
    {
        private readonly string _filePath;
        private readonly long _filePosition;
        private readonly ConcurrentBag<ArchiveChunk> _output;

        public ReadArchivedChunksJob(string filePath, long filePosition, ConcurrentBag<ArchiveChunk> output)
        {
            _filePath = filePath;
            _filePosition = filePosition;
            _output = output;
        }
        public void Execute(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            using (var fileStream = File.Open(_filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var reader = new BinaryReader(fileStream))
                {
                    reader.BaseStream.Position = _filePosition;
                    var index = reader.ReadInt64();
                    var position = reader.ReadInt64();
                    var size = reader.ReadInt64();
                    _output.Add(new ArchiveChunk(index, position, size));
                    reader.Close();
                }
                fileStream.Close();
            }
        }
    }
}