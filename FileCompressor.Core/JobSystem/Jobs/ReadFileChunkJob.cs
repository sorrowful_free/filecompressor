using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using FileCompressor.Core.Data;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public readonly struct ReadFileChunkJob : IJob
    {
        private readonly string _filePath;
        private readonly long _index;
        private readonly long _filePosition;
        private readonly long _bufferSize;
        private readonly ConcurrentBag<Chunk> _output;

        public ReadFileChunkJob(string filePath, long index, long filePosition, long bufferSize, ConcurrentBag<Chunk> output)
        {
            _filePath = filePath;
            _index = index;
            _filePosition = filePosition;
            _bufferSize = bufferSize;
            _output = output;
        }
        public void Execute(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            using (var fileStream = File.Open(_filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                fileStream.Position = _filePosition;
                var buffer = new byte[_bufferSize];
                fileStream.Read(buffer);
                _output.Add(new Chunk(_index, buffer));
                fileStream.Close();
            }
        }
    }
}