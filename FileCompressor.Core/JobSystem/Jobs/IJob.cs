using System.Threading;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public interface IJob
    {
        void Execute(CancellationToken cancellationToken);
    }
}