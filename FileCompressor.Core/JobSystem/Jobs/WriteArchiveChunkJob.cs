using System.IO;
using System.Threading;
using FileCompressor.Core.Data;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public readonly struct WriteArchiveChunkJob : IJob
    {
        private readonly string _filePath;
        private readonly long _filePosition;
        private readonly ArchiveChunk _archiveChunk;

        public WriteArchiveChunkJob(string filePath, long filePosition, ArchiveChunk archiveChunk)
        {
            _filePath = filePath;
            _filePosition = filePosition;
            _archiveChunk = archiveChunk;
        }
        public void Execute(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            using (var fileStream = File.Open(_filePath, FileMode.Open, FileAccess.Write, FileShare.Write))//if file not exist it will crashed
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    writer.BaseStream.Position = _filePosition;
                    writer.Write(_archiveChunk.Index);
                    writer.Write(_archiveChunk.Position);
                    writer.Write(_archiveChunk.Size);
                }
                fileStream.Close();
            }
        }
    }
}