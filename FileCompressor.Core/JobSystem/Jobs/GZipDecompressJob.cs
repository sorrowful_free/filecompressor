using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Threading;
using FileCompressor.Core.Data;

namespace FileCompressor.Core.JobSystem.Jobs
{
    public readonly struct GZipDecompressJob : IJob
    {
        private readonly Chunk _chunk;
        private readonly ConcurrentBag<Chunk> _output;

        public GZipDecompressJob(Chunk chunk, ConcurrentBag<Chunk> output)
        {
            _chunk = chunk;
            _output = output;
        }

        public void Execute(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            using (var input = new MemoryStream(_chunk.Data))
            {
                using (var output = new MemoryStream())
                {
                    using (var gzip = new GZipStream(input, CompressionMode.Decompress))
                    {
                        gzip.CopyTo(output);
                        gzip.Close();
                        _output.Add(new Chunk(_chunk.Index, output.ToArray()));
                    }

                    output.Close();
                }

                input.Close();
            }
        }
    }
}