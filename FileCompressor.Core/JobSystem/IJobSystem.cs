using System;
using FileCompressor.Core.JobSystem.JobHandler;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.JobSystem
{
    public interface IJobSystem : IDisposable
    {
        IJobHandler ExecuteJob(IJob job);
        
        void Complete();
        void Abort();
    }
}