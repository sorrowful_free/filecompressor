namespace FileCompressor.Core.JobSystem.JobHandler
{
    public interface IJobHandler
    {
        bool IsDone { get; }
    }
}