using System.Threading;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.JobSystem.JobHandler
{
    internal class JobHandler : IJobHandler
    {
        private readonly IJob _job;

        public bool IsDone { get; private set; }

        public JobHandler(IJob job)
        {
            _job = job;
        }

        public void ExecuteJob(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            _job.Execute(cancellationToken);
            IsDone = true;
        }
    }
}