using System;
using System.Collections.Concurrent;

namespace FileCompressor.Core.JobSystem
{
    internal class JobSystemExceptionHolder
    {
        private readonly ConcurrentBag<Exception> _exceptionList;

        public JobSystemExceptionHolder()
        {
            _exceptionList = new ConcurrentBag<Exception>();
        }

        public void CatchException(Exception exception)
        {
            _exceptionList.Add(exception);
        }

        public void RethrowException()
        {
            while (!_exceptionList.IsEmpty)
            {
                if (_exceptionList.TryTake(out var exception))
                {
                    throw exception;
                }
            }
        }
    }
}