using System;

namespace FileCompressor.Core.Processors
{
    public interface IProcessor
    {
        event Action<double> OnProgressChanged;
        void Process();
    }
}