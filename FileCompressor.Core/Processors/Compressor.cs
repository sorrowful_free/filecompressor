using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using FileCompressor.Core.Data;
using FileCompressor.Core.JobSystem;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.Processors
{
    public class Compressor : IProcessor
    {
        private readonly string _inputFile;
        private readonly string _outputFile;
        private readonly long _bufferSize;
        private readonly int _maxBatchedOperation;
        private readonly IJobSystem _jobSystem;


        private readonly long _inputSize;
        private readonly long _chunksCount;
        private readonly ConcurrentBag<Chunk> _radChunks;
        private readonly ConcurrentBag<Chunk> _compressedChunks;
        private readonly ConcurrentBag<ArchiveChunk> _archivedChunks;

        private long _readChunksCount;

        private long _archiveWritePosition;
        private long _dataWritePosition;

        protected bool IsDone => _readChunksCount >= _chunksCount;

        public event Action<double> OnProgressChanged;

        public Compressor(string inputFile, string outputFile, long bufferSize, int maxBatchedOperation,
            IJobSystem jobSystem)
        {
            _inputFile = inputFile;
            _outputFile = outputFile;
            _bufferSize = bufferSize;
            _maxBatchedOperation = maxBatchedOperation;
            _jobSystem = jobSystem;

            _inputSize = new FileInfo(_inputFile).Length;
            _chunksCount = (long) Math.Ceiling((double) _inputSize / _bufferSize);

            _radChunks = new ConcurrentBag<Chunk>();
            _compressedChunks = new ConcurrentBag<Chunk>();
            _archivedChunks = new ConcurrentBag<ArchiveChunk>();

            _dataWritePosition = ArchiveChunk.ArchiveBaseDataInBytes + _chunksCount * ArchiveChunk.ChunkSizeInBytes;
        }

        public void Process()
        {
            OnProgressChanged?.Invoke(0);
            WriteChunksCount();
            while (!IsDone)
            {
                ReadFileChunks();
                CompressChunks();
                WriteCompressedChunks();
                WriteArchivedChunks();
                OnProgressChanged?.Invoke((double) _readChunksCount / (double) _chunksCount);
                Thread.Sleep(0);
            }
        }

        private void WriteChunksCount()
        {
            if (File.Exists(_outputFile))
            {
                File.Delete(_outputFile);
            }
            using (var fileStream = File.Open(_outputFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    writer.BaseStream.Position = _archiveWritePosition;
                    writer.Write(_chunksCount);
                    writer.Write(_bufferSize);
                    _archiveWritePosition += ArchiveChunk.ArchiveBaseDataInBytes; //size of long * 2;
                    writer.Close();
                }

                fileStream.Close();
            }
        }

        private void ReadFileChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !IsDone; i++)
            {
                var position = _readChunksCount * _bufferSize;
                var size = Math.Min(_bufferSize, _inputSize - position);
                var readJob = new ReadFileChunkJob(_inputFile, _readChunksCount, position, size, _radChunks);
                _readChunksCount++;
                _jobSystem.ExecuteJob(readJob);
            }

            _jobSystem.Complete();
        }

        private void CompressChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !_radChunks.IsEmpty; i++)
            {
                if (_radChunks.TryTake(out var chunk))
                {
                    var gzipJob = new GZipCompressJob(chunk, _compressedChunks);
                    _jobSystem.ExecuteJob(gzipJob);
                }
            }

            _jobSystem.Complete();
        }

        private void WriteCompressedChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !_compressedChunks.IsEmpty; i++)
            {
                if (_compressedChunks.TryTake(out var chunk))
                {
                    var writeJob = new WriteFileChunkJob(_outputFile, _dataWritePosition, chunk, _archivedChunks);
                    _dataWritePosition += chunk.Data.Length;
                    _jobSystem.ExecuteJob(writeJob);
                }
            }

            _jobSystem.Complete();
        }

        private void WriteArchivedChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !_archivedChunks.IsEmpty; i++)
            {
                if (_archivedChunks.TryTake(out var chunk))
                {
                    var writeJob = new WriteArchiveChunkJob(_outputFile, _archiveWritePosition, chunk);
                    _archiveWritePosition += ArchiveChunk.ChunkSizeInBytes;
                    _jobSystem.ExecuteJob(writeJob);
                }
            }

            _jobSystem.Complete();
        }
    }
}