using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using FileCompressor.Core.Data;
using FileCompressor.Core.JobSystem;
using FileCompressor.Core.JobSystem.Jobs;

namespace FileCompressor.Core.Processors
{
    public class DecompressorException : Exception
    {
        public readonly string FileName;
        public DecompressorException(string filename, string message) : base(message)
        {
            FileName = filename;
        }
    }
    public class Decompressor : IProcessor
    {
        private readonly string _inputFile;
        private readonly string _outputFile;
        private readonly int _maxBatchedOperation;
        private readonly IJobSystem _jobSystem;
        
        private readonly ConcurrentBag<ArchiveChunk> _archivedChunks;
        private readonly ConcurrentBag<ArchiveChunk> _resultUncompressedArchive;
        private readonly ConcurrentBag<Chunk> _compressedChunks;
        private readonly ConcurrentBag<Chunk> _radChunks;

        private long _chunksCount;
        private long _radChunkCount;

        private long _bufferSize;

        protected bool IsDone => _radChunkCount >= _chunksCount;
        public event Action<double> OnProgressChanged;

        public Decompressor(string inputFile, string outputFile, int maxBatchedOperation, IJobSystem jobSystem)
        {
            _inputFile = inputFile;
            _outputFile = outputFile;
            _maxBatchedOperation = maxBatchedOperation;
            _jobSystem = jobSystem;
            _radChunks = new ConcurrentBag<Chunk>();
            _compressedChunks = new ConcurrentBag<Chunk>();
            _archivedChunks = new ConcurrentBag<ArchiveChunk>();
            _resultUncompressedArchive = new ConcurrentBag<ArchiveChunk>();
        }

        public void Process()
        {
            OnProgressChanged?.Invoke(0);
            ReadChunksCount();
            while (!IsDone)
            {
                ReadArchivedChunks();
                ReadFileChunks();
                DecompressChunk();
                WriteDecompressedChunks();
                OnProgressChanged?.Invoke((double) _radChunkCount / (double) _chunksCount);
                Thread.Sleep(0);
            }
        }

        private void ReadChunksCount()
        {
            using (var fileStream = File.Open(_inputFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var reader = new BinaryReader(fileStream))
                {
                    _chunksCount = reader.ReadInt64();
                    _bufferSize = reader.ReadInt64();
                    reader.Close();
                }

                fileStream.Close();
            }

            if (_chunksCount <= 0 || _bufferSize <= 0)
            {
                throw new DecompressorException(_inputFile,"trying read not archive file");
            }
        }

        private void ReadArchivedChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !IsDone; i++)
            {
                var filePosition = ArchiveChunk.ArchiveBaseDataInBytes + _radChunkCount * ArchiveChunk.ChunkSizeInBytes;
                _jobSystem.ExecuteJob(new ReadArchivedChunksJob(_inputFile, filePosition, _archivedChunks));
                _radChunkCount++;
            }

            _jobSystem.Complete();
        }

        private void ReadFileChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !_archivedChunks.IsEmpty; i++)
            {
                if (_archivedChunks.TryTake(out var archivedChunk))
                {
                    var index = archivedChunk.Index;
                    var position = archivedChunk.Position;
                    var size = archivedChunk.Size;

                    if (index < 0 || position < 0 || size < 0)
                    {
                        throw new DecompressorException(_inputFile,"trying read not archive file");
                    }
                    
                    _jobSystem.ExecuteJob(new ReadFileChunkJob(_inputFile, index, position, size, _compressedChunks));
                }
            }

            _jobSystem.Complete();
        }

        private void DecompressChunk()
        {
            for (int i = 0; i < _maxBatchedOperation && !_compressedChunks.IsEmpty; i++)
            {
                if (_compressedChunks.TryTake(out var chunk))
                {
                    _jobSystem.ExecuteJob(new GZipDecompressJob(chunk, _radChunks));
                }
            }
            
            _jobSystem.Complete();
        }

        private void WriteDecompressedChunks()
        {
            for (int i = 0; i < _maxBatchedOperation && !_radChunks.IsEmpty; i++)
            {
                if (_radChunks.TryTake(out var chunk))
                {
                    var filePosition = chunk.Index * _bufferSize;
                    _jobSystem.ExecuteJob(new WriteFileChunkJob(_outputFile, filePosition, chunk,
                        _resultUncompressedArchive));
                }
            }

            _jobSystem.Complete();
        }
    }
}