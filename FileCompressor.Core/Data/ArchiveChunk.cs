namespace FileCompressor.Core.Data
{
    public readonly struct ArchiveChunk
    {
        public const int ArchiveBaseDataInBytes = 8 * 2;//sizeof(long)*2
        public const int ChunkSizeInBytes = 8 * 3; //sizeof(long)*3
        
        public readonly long Index;
        public readonly long Position;
        public readonly long Size;
        public ArchiveChunk(long index, long position, long size)
        {
            Index = index;
            Position = position;
            Size = size;
        }
    }
}