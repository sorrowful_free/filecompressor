namespace FileCompressor.Core.Data
{
    public readonly struct Chunk
    {
        public readonly long Index;
        public readonly byte[] Data;

        public Chunk(long index, byte[] data)
        {
            Index = index;
            Data = data;
        }
    }
}