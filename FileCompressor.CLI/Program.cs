﻿using System;
using System.Diagnostics;
using System.Threading;
using FileCompressor.CLI.CLI;
using FileCompressor.Core.JobSystem;
using FileCompressor.Core.Processors;


namespace FileCompressor.CLI
{
    class Program
    {
        private static CancellationTokenSource _userCancellationTokenSource;

        static void Main(string[] args)
        {
            _userCancellationTokenSource = new CancellationTokenSource();
            Console.CancelKeyPress += ConsoleOnCancelKeyPressHandler;
            try
            {
                var consoleArguments = new ConsoleArguments(args);
                switch (consoleArguments.ApplicationMode)
                {
                    default:
                    case ApplicationMode.Unknown:
                        ConsoleInterface.PrintHelp();
                        break;
                    case ApplicationMode.Compress:
                        Compress(consoleArguments.InputFilePath,
                            consoleArguments.OutputFilePath,
                            consoleArguments.ThreadCount,
                            consoleArguments.BufferSize,
                            _userCancellationTokenSource.Token);
                        break;
                    case ApplicationMode.Decompress:
                        Decompress(consoleArguments.InputFilePath,
                            consoleArguments.OutputFilePath,
                            consoleArguments.ThreadCount,
                            _userCancellationTokenSource.Token);
                        break;
                }
            }
            catch (Exception exception)
            {
                ConsoleInterface.HandleException(exception);
            }
            finally
            {
                Console.CancelKeyPress -= ConsoleOnCancelKeyPressHandler;
                Console.ReadKey();
            }
        }

        private static void ConsoleOnCancelKeyPressHandler(object sender, ConsoleCancelEventArgs e)
        {
            _userCancellationTokenSource.Cancel();
        }

        private static void Decompress(string inputFilePath, string outputFilePath, int threadsCount,
            CancellationToken userCancellationToken)
        {
            using (var jobSystem = new JobSystem(threadsCount, userCancellationToken))
            {
                var stopwatch = Stopwatch.StartNew();
                var decompressor = new Decompressor(inputFilePath, outputFilePath, threadsCount, jobSystem);
                decompressor.OnProgressChanged += ConsoleInterface.ProgressPrint;
                decompressor.Process();
                jobSystem.Complete();
                decompressor.OnProgressChanged -= ConsoleInterface.ProgressPrint;
                stopwatch.Stop();
                ConsoleInterface.PrintResult(stopwatch.Elapsed);
            }
        }

        private static void Compress(string inputFilePath, string outputFilePath, int threadsCount, int bufferSize,
            CancellationToken userCancellationToken)
        {
            using (var jobSystem = new JobSystem(threadsCount, userCancellationToken))
            {
                var stopwatch = Stopwatch.StartNew();
                var compressor = new Compressor(inputFilePath, outputFilePath, bufferSize, threadsCount, jobSystem);
                compressor.OnProgressChanged += ConsoleInterface.ProgressPrint;
                compressor.Process();
                jobSystem.Complete();
                compressor.OnProgressChanged -= ConsoleInterface.ProgressPrint;
                stopwatch.Stop();
                ConsoleInterface.PrintResult(stopwatch.Elapsed);
            }
        }
    }
}