using System;
using System.IO;

namespace FileCompressor.CLI.CLI
{
    public enum ApplicationMode
    {
        Unknown,
        Compress,
        Decompress
    }

    public class ConsoleArgumentException : Exception
    {
        public ConsoleArgumentException(string message) : base(message)
        {
        }
    }

    public class ConsoleApplicationModeException : Exception
    {
        public ConsoleApplicationModeException(string message) : base(message)
        {
        }
    }

    public class ConsoleArguments
    {
        public readonly ApplicationMode ApplicationMode;
        public readonly string InputFilePath;
        public readonly string OutputFilePath;
        public readonly int ThreadCount;
        public readonly int BufferSize;

        public ConsoleArguments(string[] args)
        {
            var isCompress = false;
            var isDecompress = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (TryParseBoolArg(args, i, "--compress", "-c"))
                {
                    isCompress = true;
                }

                if (TryParseBoolArg(args, i, "--decompress", "-d"))
                {
                    isDecompress = true;
                }

                if (TryParseStringArg(args, i, out var inputFilePath, "--input", "-i"))
                {
                    InputFilePath = inputFilePath;

                    i++;
                    continue;
                }

                if (TryParseStringArg(args, i, out var outputFilePath, "--output", "-o"))
                {
                    OutputFilePath = outputFilePath;
                    i++;
                    continue;
                }

                if (TryParseIntArg(args, i, out var threadCount, "--threads", "-t"))
                {
                    ThreadCount = threadCount;
                    i++;
                    continue;
                }

                if (TryParseIntArg(args, i, out var bufferSize, "--buffer_size", "-b"))
                {
                    BufferSize = bufferSize;
                    i++;
                    continue;
                }
            }

            ApplicationMode = isCompress ? ApplicationMode.Compress
                : isDecompress ? ApplicationMode.Decompress
                : ApplicationMode.Unknown;

            ThreadCount = ThreadCount <= 0 ? Environment.ProcessorCount * 2 : ThreadCount;
            BufferSize = BufferSize <= 0 ? 1024 * 1024 : BufferSize;

            if (ApplicationMode == ApplicationMode.Unknown)
            {
                throw new ConsoleApplicationModeException($"invalid command line arguments");
            }

            if (!IsValidInputFile(InputFilePath))
            {
                throw new ConsoleArgumentException($"invalid input file path: {InputFilePath}");
            }

            if (!IsValidPath(OutputFilePath))
            {
                throw new ConsoleArgumentException($"invalid output file path: {OutputFilePath}");
            }
        }


        private static bool TryParseBoolArg(string[] args, int argIndex, params string[] argTemplates)
        {
            var arg = args[argIndex];
            var result = false;
            for (int i = 0; i < argTemplates.Length; i++)
            {
                var argTemplate = argTemplates[i];
                result |= string.Equals(arg, argTemplate, StringComparison.OrdinalIgnoreCase);
                if (result)
                {
                    break;
                }
            }

            return result;
        }

        private static bool TryParseStringArg(string[] args, int argIndex, out string value,
            params string[] argTemplates)
        {
            if (TryParseBoolArg(args, argIndex, argTemplates))
            {
                var argValueIndex = argIndex + 1;

                if (argValueIndex < args.Length)
                {
                    var argValue = args[argValueIndex];
                    value = argValue;
                    return true;
                }
            }

            value = null;
            return false;
        }

        private static bool TryParseIntArg(string[] args, int argIndex, out int value, params string[] argTemplates)
        {
            if (TryParseStringArg(args, argIndex, out var stringValue, argTemplates))
            {
                if (int.TryParse(stringValue, out value))
                {
                    return true;
                }
            }

            value = -1;
            return false;
        }

        private static bool IsValidPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }

            var fileInfo = new FileInfo(path);
            var directoryInfo = fileInfo.Directory;
            return directoryInfo?.Exists ?? false || fileInfo.Exists;
        }

        private static bool IsValidInputFile(string inputFilePath)
        {
            if (IsValidPath(inputFilePath))
            {
                return File.Exists(inputFilePath);
            }

            return false;
        }
    }
}