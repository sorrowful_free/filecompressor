using System;
using System.IO;
using FileCompressor.Core.Processors;

namespace FileCompressor.CLI.CLI
{
    public class ConsoleInterface
    {
        public static void PrintHelp()
        {
            Console.Write("use argument -d --decompress for run application for decompression file\n" +
                          "use argument -c --compress for run application for compression file\n" +
                          "for compression and decompression need specify arguments:\n" +
                          "\t -i --input for specify input file path\n" +
                          "\t -o --output for specify output output file path\n" +
                          "\t -t --threads for specify threads count\n" +
                          "\t -b --buffer_size for specify max chunk buffer size for compression (in bytes default 1mb or 1024 * 1024b)\n");
        }

        public static void ProgressPrint(double progress)
        {
            var allProgressCharsCount = 35;
            var normalizedProgress = (int) (progress * allProgressCharsCount);
            if (progress > 0 && progress <= 1)
            {
                Console.CursorTop--;
            }

            Console.Write("current progress:[");
            for (int i = 0; i < normalizedProgress; i++)
            {
                Console.Write("#");
            }

            for (int i = 0; i < allProgressCharsCount - normalizedProgress; i++)
            {
                Console.Write("-");
            }

            Console.WriteLine("]");
        }

        public static void PrintResult(TimeSpan timeSpan)
        {
            Console.WriteLine($"done by:{timeSpan}");
        }

        public static void HandleException(Exception exception)
        {
            switch (exception)
            {
                case FileNotFoundException fileNotFoundException:
                    PrintInputFileUnavailable(fileNotFoundException);
                    break;
                case DecompressorException decompressorException:
                    PrintInputFileIsNotArchive(decompressorException);
                    break;
                case ConsoleApplicationModeException consoleApplicationModeException:
                    PrintHelp();
                    break;
                case ConsoleArgumentException consoleArgumentException:
                    PrintConsoleArgumentError(consoleArgumentException);
                    break;
                default:
                    PrintUnknownException(exception);
                    break;
            }
        }

        private static void PrintInputFileUnavailable(FileNotFoundException fileNotFoundException)
        {
            Console.WriteLine($"file \"{fileNotFoundException.FileName}\" unavailable(or not exist) please check path of input file");
        }

        private static void PrintInputFileIsNotArchive(DecompressorException decompressorException)
        {
            Console.WriteLine($"file \"{decompressorException.FileName}\" is not archive or archive was broken");
        }
        
        private static void PrintConsoleArgumentError(ConsoleArgumentException consoleArgumentException)
        {
            Console.WriteLine(consoleArgumentException.Message);
        }

        private static void PrintUnknownException(Exception exception)
        {
            Console.WriteLine($"unknown error with message:{exception.Message}");
        }
    }
}