using System;
using NUnit.Framework;

namespace Tests
{
    [Order(2)]
    public class StressTests
    {
        private BasicTests _basicTests;

        [SetUp]
        public void SetupTest()
        {
            _basicTests = new BasicTests();
        }

        [Test, Order(1)]
        public void TryCompress10Times()
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    _basicTests.CompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test, Order(2)]
        public void TryDecompress10Times()
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    _basicTests.DecompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test, Order(3)]
        public void TryCompressAndDecompress10Times()
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    _basicTests.CompressAndDecompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test, Order(4)]
        public void TryCompress100Times()
        {
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    _basicTests.CompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }


        [Test, Order(5)]
        public void TryDecompress100Times()
        {
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    _basicTests.DecompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }


        [Test, Order(6)]
        public void TryCompressAndDecompress100Times()
        {
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    _basicTests.CompressAndDecompressInternal();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}