using System;
using System.IO;
using System.Linq;
using System.Threading;
using FileCompressor.Core.JobSystem;
using FileCompressor.Core.Processors;
using NUnit.Framework;

namespace Tests
{
    [Order(1)]
    public class BasicTests
    {
        public const string InputFilePath = "input/test4.mov";
        public const string ArchiveFilePath = "input/arc/test_4_arch.mov";
        public const string OutputFilePath = "input/out/test_4_uncompress.mov";
        public const int ThreadsCount = 8;
        public const int BufferSize = 1024 * 1024;

        public void CompressInternal()
        {
            using (var jobSystem = new JobSystem(ThreadsCount, new CancellationToken()))
            {
                var compressor = new Compressor(InputFilePath, ArchiveFilePath, BufferSize, ThreadsCount, jobSystem);

                compressor.Process();
                jobSystem.Complete();
            }
        }

        public void DecompressInternal()
        {
            using (var jobSystem = new JobSystem(ThreadsCount, new CancellationToken()))
            {
                var decompressor = new Decompressor(ArchiveFilePath, OutputFilePath, ThreadsCount, jobSystem);

                decompressor.Process();
                jobSystem.Complete();
            }
        }

        public void CompressAndDecompressInternal()
        {
            CompressInternal();
            DecompressInternal();
        }

        [Test, Order(1)]
        public void Compress()
        {
            try
            {
                CompressInternal();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test, Order(2)]
        public void Decompress()
        {
            try
            {
                DecompressInternal();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Test, Order(3)]
        public void CompressAndDecompress()
        {
            try
            {
                CompressAndDecompressInternal();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            Assert.Pass();
        }
    }
}